# StepProject_Forkio

StepProject_Forkio by Babur Timur and Iaroshevskiy Volodymyr

Девелопери:

Iaroshevskiy Volodymyr - Студент - 1

Babur Timur - Студент - 2

Завдання для студента №1:
Зверстати шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана.
Зверстати секцію People Are Talking About Fork.

Завдання для студента №2:
Зверстати блок Revolutionary Editor. Кнопки треба зробити, щоб виглядали як тут справа вгорі (звідти ж за допомогою інспектора можна взяти всі SVG іконки і завантажити стилі, що використовуються на гітхабі).
Зверстати секцію Here is what you get.
Зверстати секцію Fork Subscription Pricing. У блоці з цінами третій елемент завжди буде "виділений" і буде більшим за інші (тобто не по кліку/ховеру, а статично). (Сказали краще зробити по ховеру)

Список використаних технологій:
Gulp,
Sass (Scss),
HTML5,
CSS3,
JavaScript ES6,
BEM Methodology.